// c64pm.js

function update_recommendation() {
    update_facts();
}

function update_facts() {
    var li=document.getElementById( "facts" );
    var value;
    
    li.innerHTML = "";
    
    value = document.getElementById( "sprite_count" ).value;
    if ( value )
	li.innerHTML += "<li> Sprites needed : " + value + "</li>\n";

    value = document.getElementById( "hires").checked
    if ( value )
	li.innerHTML += "<li> Hi-Res Image(s) : " + value + "</li>\n";

    value = document.getElementById( "basic_rom").checked
    if ( value )
	li.innerHTML += "<li> BASIC ROM : " + value + "</li>\n";

    value = document.getElementById( "kernel_rom").checked
    if ( value )
	li.innerHTML += "<li> Kernel ROM : " + value + "</li>\n";
}
